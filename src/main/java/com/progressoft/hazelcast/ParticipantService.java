package com.progressoft.hazelcast;

import com.hazelcast.core.IMap;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class ParticipantService {
    private final ParticipantMapper mapper;
    private final IMap<String, Participant> customersCache;

    public Participant get(String code) {
        return customersCache.get(code);
    }

    public Participant add(ParticipantRequest request) {
        return customersCache.put(request.getCode(), mapper.toModel(request));
    }

    public Participant update(String code, ParticipantRequest request) {
        return customersCache.put(code, mapper.toModel(request));
    }
}
