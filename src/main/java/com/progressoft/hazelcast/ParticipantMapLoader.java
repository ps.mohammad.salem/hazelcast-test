package com.progressoft.hazelcast;

import com.hazelcast.core.MapStore;
import lombok.AllArgsConstructor;

import java.util.Collection;
import java.util.Map;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@AllArgsConstructor
public class ParticipantMapLoader implements MapStore<String, Participant> {
    private final JpaParticipantEntityRepository jpaParticipantEntityRepository;
    private final ParticipantMapper participantMapper;

    @Override
    public Participant load(String code) {
        return jpaParticipantEntityRepository.findByCode(code)
                .map(participantMapper::toModel)
                .orElse(null);
    }

    @Override
    public Map<String, Participant> loadAll(Collection<String> collection) {
        return collection.stream().collect(toMap(identity(), this::load));
    }

    @Override
    public Iterable<String> loadAllKeys() {
        return jpaParticipantEntityRepository.findAll().stream()
                .map(ParticipantEntity::getCode)
                .collect(toList());
    }

    @Override
    public void store(String code, Participant value) {
        System.out.println("write through called");
        save(code, value);
    }

    @Override
    public void storeAll(Map<String, Participant> map) {
        System.out.println("write behind called with " + map.size() + " entries");
        map.forEach(this::save);
    }

    private void save(String code, Participant value) {
        ParticipantEntity participantEntity = jpaParticipantEntityRepository.findByCode(code)
                .map(p -> {
                    p.setName(value.getName());
                    p.setBic(value.getBic());
                    return p;
                })
                .orElseGet(() -> participantMapper.toEntity(value));
        jpaParticipantEntityRepository.save(participantEntity);
    }

    @Override
    public void delete(String code) {
        jpaParticipantEntityRepository
                .findByCode(code)
                .ifPresent(jpaParticipantEntityRepository::delete);
    }

    @Override
    public void deleteAll(Collection<String> keys) {
        keys.forEach(this::delete);
    }
}
