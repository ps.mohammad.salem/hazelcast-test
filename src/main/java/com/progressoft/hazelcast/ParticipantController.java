package com.progressoft.hazelcast;

import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AllArgsConstructor
@RestController
public class ParticipantController {
    private final ParticipantService service;
    private final JpaParticipantEntityRepository repository;
    private final ParticipantMapper mapper;

    @GetMapping("{code}")
    public Participant get(@PathVariable("code") String code) {
        return service.get(code);
    }

    @RateLimiter(name = "rate")
    @GetMapping("/rate/{code}")
    public Participant rate(@PathVariable("code") String code) {
        return service.get(code);
    }

    @Bulkhead(name = "bulkhead")
    @GetMapping("/bulkhead/{code}")
    public Participant bulkhead(@PathVariable("code") String code) {
        return service.get(code);
    }

    @GetMapping("/db/{code}")
    public Participant getFromDb(@PathVariable("code") String code) {
        return repository.findByCode(code).map(mapper::toModel).orElse(null);
    }

    @PostMapping
    public Participant post(@Valid @RequestBody ParticipantRequest request) {
        return service.add(request);
    }

    @PutMapping("{code}")
    public Participant put(@PathVariable("code") String code, @Valid @RequestBody ParticipantRequest request) {
        return service.update(code, request);
    }
}
