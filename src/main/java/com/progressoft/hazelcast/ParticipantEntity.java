package com.progressoft.hazelcast;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class ParticipantEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String code;
    private String name;
    private String bic;
}
