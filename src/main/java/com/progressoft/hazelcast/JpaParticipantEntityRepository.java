package com.progressoft.hazelcast;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JpaParticipantEntityRepository extends JpaRepository<ParticipantEntity, Integer> {
    Optional<ParticipantEntity> findByCode(String code);
}
