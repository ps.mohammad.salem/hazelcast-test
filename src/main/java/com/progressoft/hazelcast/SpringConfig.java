package com.progressoft.hazelcast;

import com.hazelcast.config.*;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.hazelcast.config.EvictionPolicy.LRU;
import static com.hazelcast.config.MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE;

@Configuration
public class SpringConfig {
    @Bean
    public ParticipantMapLoader participantMapLoader(
            JpaParticipantEntityRepository participantRepository,
            ParticipantMapper participantMapper){
        return new ParticipantMapLoader(participantRepository, participantMapper);
    }

    @Bean
    public MapStoreConfig participantMapStoreConfig(
            @Value("${writeDelaySeconds:2}") int writeDelaySeconds,
            @Value("${writeBatchSize:500}") int writeBatchSize,
            ParticipantMapLoader participantMapLoader) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setImplementation(participantMapLoader);
        mapStoreConfig.setWriteDelaySeconds(writeDelaySeconds);
        mapStoreConfig.setWriteBatchSize(writeBatchSize);
        return mapStoreConfig;
    }

    @Bean
    public Config hazelCastConfig(MapStoreConfig participantMapStoreConfig) {
        NetworkConfig networkConfig = new NetworkConfig();
        networkConfig.getJoin().getMulticastConfig().setEnabled(true);
        return new Config()
                .setInstanceName("hazelcast-instance")
                .addMapConfig(new MapConfig()
                        .setName("customers")
                        .setMaxSizeConfig(new MaxSizeConfig(200, FREE_HEAP_SIZE))
                        .setEvictionPolicy(LRU)
                        .setTimeToLiveSeconds(-1)
                        .setMapStoreConfig(participantMapStoreConfig))
                .setNetworkConfig(networkConfig);
    }

    @Bean
    public IMap<String, Participant> customersCache(
            @Qualifier("hazelcastInstance") HazelcastInstance hazelcastInstance) {
        return hazelcastInstance.getMap("customers");
    }
}
