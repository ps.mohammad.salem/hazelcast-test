build:
	mvn clean install
run:
	java -jar target/demo-0.0.1-SNAPSHOT.jar
run-write-behind:
	java -DwriteDelaySeconds=2 -DwriteBatchSize=500 -jar target/demo-0.0.1-SNAPSHOT.jar
run-write-through:
	java -DwriteDelaySeconds=0 -DwriteBatchSize=1 -jar target/demo-0.0.1-SNAPSHOT.jar
docker:
	mvn spring-boot:build-image -Dspring-boot.build-image.imageName=hazelcast-test
docker-run:
	docker run -p 9090:9090 -t hazelcast-test
init:
	curl -d '{ "code" : "code", "name" : "name", "bic" : "bic" }' -H 'Content-Type: application/json' localhost:9090
post:
	k6 run k6/post.js
load:
	k6 run k6/get-cache.js
rate:
	k6 run k6/get-rate.js
bulkhead:
	k6 run k6/get-bulkhead.js