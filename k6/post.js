import http from 'k6/http';
import {check} from 'k6';

export let options = {
    stages: [
        { duration: '05s', target: 20 },
        { duration: '20s', target: 20 },
        { duration: '05s', target: 0 },
    ],
};

export default function() {
    var payload = JSON.stringify({
        code: uuidv4(),
        name: 'name',
        bic: 'bic'
    });

    var params = {
        headers: {
            'Content-Type': 'application/json',
        },
    };
    let res = http.post('http://localhost:9090/', payload, params);
    check(res, { 'status was 200': r => r.status === 200 });
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

