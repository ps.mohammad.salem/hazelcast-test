import http from 'k6/http';
import { check } from 'k6';

export let options = {
  stages: [
    { duration: '05s', target: 20 },
    { duration: '20s', target: 20 },
    { duration: '05s', target: 0 },
  ],
};

export default function() {
  let res = http.get('http://localhost:9090/bulkhead/code');
  check(res, { 'status was 200': r => r.status === 200 });
}
